
import {Controller} from "../components/Controller";
import * as express from 'express';
import {HttpServer} from "../components/HttpServer";

export class WarehouseController extends Controller {

    constructor(server: HttpServer){
        super(server);
    }

    static warehouses = [
        { id: 1, name: 'A'},
        { id: 2, name: 'B'}
    ];

    register() {
        console.log('hey');
        let router = express.Router();
        router.get('/', this.get.bind(this));
        this._server.app.use('/api/warehouse', router);
    }

    get(req: express.Request, res: express.Response, next: express.NextFunction) {
        res.send('hello world');
    }
}