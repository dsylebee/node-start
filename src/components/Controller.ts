import {HttpServer} from "./HttpServer";

// interface to respect.
export interface IController
{
    register() : void;
}

// base controller.
export abstract class Controller implements IController
{
    protected _server: HttpServer;
    constructor(server: HttpServer) {
        this._server = server;
        this.register();
    }

    abstract register() : void;
}
