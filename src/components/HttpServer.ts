import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as http from 'http';
import {Controller} from "./Controller";

export class HttpServer {
    app: express.Application;
    server: http.Server;
    port: number | string;
    controllers: Array<Controller>;

    constructor() {
        this.app = express();
        this.expressConfiguration();
    }

    establishPort() {
        this.port = process.env['PORT'] || 3030;
        this.app.set('port', this.port);
    }

    createHttpServer() {
        this.server = http.createServer(this.app as any);
        this.server.listen(this.port);
        this.server.on('error', this.onError.bind(this));
        this.server.on('listening', this.onListening.bind(this));
    }

    run() {
        this.establishPort();
        this.createHttpServer();
    }

    setControllers(controllers: Array<any>) {
        this.controllers = controllers.map(t => new t(this));
    }

    onError(error: any) {

        if (error.syscall !== "listen")
            throw error;

        let bind = typeof this.port === 'string' ? `Pipe ${this.port}` : `Port ${this.port}`;
        switch (error.code) {
            case "EACCES":
                console.error(bind + " requires elevated privileges");
                process.exit(1);
                break;
            case "EADDRINUSE":
                console.error(bind + " is already in use");
                process.exit(1);
                break;
            default:
                throw error;
        }
    }

    onListening() {
        let address = this.server.address();
        let bind = typeof address === "string" ? `pipe ${address}` : `port ${address.port}`;
        console.log(`Listening on ${bind}`);
    }

    expressConfiguration() {
        this.app.use(bodyParser.urlencoded({extended: true}));
        this.app.use(bodyParser.json());
        this.app.use(this.notFound.bind(this));
    }

    notFound(err: any, req: express.Request, res: express.Response, next: express.NextFunction) {
        let error = new Error("Not Found");
        err.status = 404;
        next(err);
    }
}