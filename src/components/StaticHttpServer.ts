import {HttpServer} from "./HttpServer";

export class StaticHttpServer extends HttpServer {

    private static _instance: StaticHttpServer;

    static instance() {
        if (StaticHttpServer._instance == null)
            StaticHttpServer._instance = new StaticHttpServer();

        return StaticHttpServer._instance;
    }
}