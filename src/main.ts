import {StaticHttpServer} from "./components/StaticHttpServer";
import {WarehouseController} from "./controllers/Warehouse";

// controllers
let controllers =[
    WarehouseController
];

// server.
let server = StaticHttpServer.instance();

// run.
server.setControllers(controllers);
server.run();